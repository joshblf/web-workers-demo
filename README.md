## Web Workers POC

This project is a simple Web Workers POC.  It demonstrates how you can run multiple javascript files on separate threads to speed up page execution.

### Prerequisites
- `npm i -g local-web-server`

### Running the project
- run `ws` in main project directory to start web server.
- Open browser that supports Web Workers and navigate to url provided in console

### Tips
- The times logged in the console show the order the requests are returned in.
