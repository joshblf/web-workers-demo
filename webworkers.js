onmessage = function(e) {
    fetch('https://jsonplaceholder.typicode.com/posts', { 
        headers: {'Access-Control-Allow-Origin':'*'}
    }).then(function(res) {
        if (res.ok) {
            return res.json().then(function(data) {
                postMessage(data)
            })
        }
    }).catch(function(err) {
        console.log(err.message)
    })
}